import { Injectable } from "@angular/core";
import { Observable, Subject, of } from "rxjs";
import { HttpClient, HttpErrorResponse } from "@angular/common/http";


@Injectable()
export class FollowingService {

  public refreshData = new Subject();
  public refreshAllPost = new Subject();

  public identifier = new Subject();
  public userIdentifier = new Subject();

  //end class
}
