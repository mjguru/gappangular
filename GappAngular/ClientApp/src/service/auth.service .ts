import { Injectable } from "@angular/core";
import { Observable, Subject, of } from "rxjs";
import { HttpClient, HttpErrorResponse } from "@angular/common/http";


@Injectable()
export class AuthenticationService {

  public isLoggedIn = new Subject(); 
   
  login(httpClient: HttpClient, baseUrl: string, username: string, password: string): Observable<ResultVM> {

    let formData = new FormData();
    formData.append('username', username);
    formData.append('password', password);

    return httpClient.post<ResultVM>(baseUrl + 'api/gappdata/ValidateLogin', formData)
      .pipe(err => this.handleError(err));
  }

  handleError(err): Observable<ResultVM> {
    console.log(err);
    return err;
  }


  //end class
}

interface ResultVM {  
  resultCode: string,
  returnData: string

}
