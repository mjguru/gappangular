import { Component, Inject, OnInit, Input } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { FormControl, FormGroup, FormBuilder, Validators } from '@angular/forms'
import { Router } from '@angular/router';
import { FollowingService } from '../../service/following.service';

@Component({
  selector: 'app-followcount',
  templateUrl: './followcount.component.html'
})

export class FollowCountComponent implements OnInit {

  @Input() userIdentifier: string;
   

  waiting = false; 

  public followers: FollowerVM[];

  totalFollowersString: string;

  ngOnInit() {
       
    this.followingService.refreshData.subscribe(refresh => this.onServiceResult(refresh));
    this.followingService.userIdentifier.subscribe(userIdentifier => this.onServiceResult(userIdentifier));

    this.loadData();
  }

  constructor(private followingService: FollowingService, private http: HttpClient, @Inject('BASE_URL') private baseUrl: string) {
      

  }

  onServiceResult(identifier) {
     
    if (identifier) {

      this.userIdentifier = identifier;

      this.waiting = false; 
      this.followers = [];

      this.loadData();
    }
  }

  loadData() {

    let params = new HttpParams();

    params = params.append('userIdentifer', this.userIdentifier);

    this.waiting = true;

    this.http.get<FollowerVM[]>(this.baseUrl + 'api/gappdata/GetFollowedByCount', { params: params }).subscribe(result => {

      this.totalFollowersString =  "0 followers";

      if (result != null && result.length > 0) {
        let total = result[0].rowCount;
        if (total > 1) {
          this.totalFollowersString = result[0].rowCount.toString() + " followers";
        }
        else if (total == 1) {
          this.totalFollowersString = result[0].rowCount.toString() + " follower";
        }
        else {
          this.totalFollowersString = result[0].rowCount.toString() + " followers";
        }
        
      }
      
      this.waiting = false;


    }, error => console.error(error));
  }
   

  //end class
}

interface FollowerVM {

  username: string,
  followingSince: string,
  userIdentifier: string,
  rowCount: number

}
