import { Component, Inject, OnInit } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { FormControl, FormGroup, FormBuilder, Validators } from '@angular/forms'

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html'
})

export class RegisterComponent implements OnInit {


  resultVM: ResultVM;

  registerForm: FormGroup;
  submitted = false;
  waiting = false;
  termsAgreed = true;
   
  constructor(private formBuilder: FormBuilder, private http: HttpClient, @Inject('BASE_URL') private baseUrl: string) {
     
  }


  ngOnInit() {

    this.resultVM = { ResultCode: '', ReturnData: '' };

    this.registerForm = this.formBuilder.group({
        username: ['', [Validators.required, Validators.minLength(4)]],
        password: ['', [Validators.required, Validators.minLength(6)]],
        confirmpassword: ['', [Validators.required, Validators.minLength(6)]],
         checkboxTerms: [false, Validators.required]
      },
      { validator: this.checkIfMatchingPasswords('password', 'confirmpassword') }
    );
     
  }

  get result() {
    return this.resultVM;
  }

  get f()
  {
    return this.registerForm.controls;
  }

  checkIfMatchingPasswords(passwordKey: string, passwordConfirmationKey: string) {
    return (group: FormGroup) => {
      let passwordInput = group.controls[passwordKey],
        passwordConfirmationInput = group.controls[passwordConfirmationKey];
      if (passwordInput.value !== passwordConfirmationInput.value) {
        return passwordConfirmationInput.setErrors({ notEquivalent: true })
      }
      else {
        return passwordConfirmationInput.setErrors(null);
      }
    }
  }

  registerClick() {

    this.submitted = true;
    var formVal = this.registerForm.value;

    // stop here if form is invalid
    if (this.registerForm.invalid) {
      return;
    }

    this.termsAgreed = true;
    
    if (formVal.checkboxTerms == false) {
      this.termsAgreed = false;
      return;
    }

    this.waiting = true;

    // Initialize Params Object
    let formData = new FormData();

    formData.append('username', formVal.username);
    formData.append('password', formVal.password);

    this.http.post<ResultVM>(this.baseUrl + 'api/gappdata/CreateNewUser', formData)
      .subscribe(result => {
        this.resultVM = result;
        this.waiting = false;
      }, error => console.error(error));



  }

  copyToClipboard(val) {
    
    const selBox = document.createElement('textarea');
    selBox.style.position = 'fixed';
    selBox.style.left = '0';
    selBox.style.top = '0';
    selBox.style.opacity = '0';
    selBox.value = val;
    document.body.appendChild(selBox);
    selBox.focus();
    selBox.select();
    document.execCommand('copy');
    document.body.removeChild(selBox);
  }


   //end class
}

 


interface ResultVM {
  ResultCode: string,
  ReturnData: string 

}
