import { Component, Inject, OnInit } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { FormControl, FormGroup, FormBuilder, Validators } from '@angular/forms'
import { Router } from '@angular/router';
import { AuthenticationService } from '../../service/auth.service ';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html'
})

export class LoginComponent implements OnInit {


  resultVM: ResultVM;

  loginForm: FormGroup;
  submitted = false;
  waiting = false; 
   
  public authService: AuthenticationService;

  constructor(auth: AuthenticationService, private router: Router, private formBuilder: FormBuilder,
    private http: HttpClient, @Inject('BASE_URL') private baseUrl: string) {
         
    this.authService = auth; 
  }


  ngOnInit() {

    this.resultVM = { resultCode: '', returnData: '' };

    this.loginForm = this.formBuilder.group({
      username: ['', [Validators.required]],
      password: ['', [Validators.required]]
    }
    );

  }

  get result() {
    return this.resultVM;
  }

  get f() {
    return this.loginForm.controls;
  }
   

  loginClick() {
 
    this.submitted = true;
    var formVal = this.loginForm.value;

    // stop here if form is invalid
    if (this.loginForm.invalid) {
      return;
    }
      
    this.waiting = true;
    this.authService.login(this.http, this.baseUrl,  formVal.username, formVal.password).subscribe(loginResult =>
      this.onLoginResult(loginResult)       
    );

  }

  onLoginResult(serviceResult) {

    this.resultVM = serviceResult;

    if (this.resultVM.resultCode == "1") {
      this.authService.isLoggedIn.next(true);
     // location.href = '/';
      this.router.navigate(['/']);
    }
    else {
      this.authService.isLoggedIn.next(false);
      this.waiting = false; 
    }

    
  }
  //end class
}




interface ResultVM {
  resultCode: string,
  returnData: string

}
