

import { Component, Inject, Input } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { FollowingService } from '../../service/following.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-userpost',
  templateUrl: './userpost.component.html'
})
export class UserPostComponent {
   

  waiting = false;
  totalRecords = 0;
  pageNumber = 1;
  rowCount = 0;
  userIdentifer: string;

  public posts: Post[];

  ngOnInit() {

    this.waiting = true;

    this.route.params.subscribe(
      p => {
        this.userIdentifer = p['id'];

        this.totalRecords = 0;
        this.pageNumber = 1;
        this.rowCount = 0;
        this.posts = [];

        this.loadData();
      }
    );


  }
  constructor(private route: ActivatedRoute, private router: Router, private followingService: FollowingService,
    private http: HttpClient, @Inject('BASE_URL') private baseUrl: string) {
     
  }


  loadData() {

    let params = new HttpParams();

    params = params.append('userIdentifer', this.userIdentifer);

    this.waiting = true;

    this.http.get<Post[]>(this.baseUrl + 'api/gappdata/GetPostByFilters', { params: params }).subscribe(result => {

      if (this.posts == undefined) {
        this.posts = result;
      }
      else {
        for (var i = 0, len = result.length; i < len; i++) {
          this.posts.push(result[i]);
        }
      }

      this.totalRecords = this.posts.length;
      this.pageNumber += 1;


      if (this.posts.length > 0) {
        this.rowCount = this.posts[0].rowCount;
      }
      this.waiting = false;

    }, error => console.error(error));
  }

  loadMoreData() {
    this.loadData();
  }

  //end class
}

interface Post {

  createdUserIdentifier: string,
  postIdentifier: string,
  postTitle: string,
  postContent: string,
  createdOn: string,
  createdByUser: string,
  rowCount: number

}
