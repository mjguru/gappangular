import { Component, Inject, OnInit } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { FormControl, FormGroup, FormBuilder, Validators } from '@angular/forms'
import { Router } from '@angular/router';
import { FollowingService } from '../../service/following.service';

@Component({
  selector: 'app-addpost',
  templateUrl: './addpost.component.html'
})
export class AddPostComponent implements OnInit {

  displayName: string; 
  resultVM: ResultVM;
  addPostForm: FormGroup;
  submitted = false;
  waiting = false;
   

  constructor(private followingService: FollowingService, private router: Router, private formBuilder: FormBuilder,
          private http: HttpClient, @Inject('BASE_URL') private baseUrl: string) {
     

    this.http.get<UserInfo>(baseUrl + 'api/gappdata/GetLoggedInUserInfo')
      .subscribe(result => {
        this.displayName = result.displayName;
      }, error => console.error(error));


  }

 

  ngOnInit() {

    this.resultVM = { resultCode: '', returnData: '' };

    this.addPostForm = this.formBuilder.group({
      comments: ['', [Validators.required, Validators.minLength(100)]]
    }
    );

  }

  get result() {
    return this.resultVM;
  }

  get f() {
    return this.addPostForm.controls;
  }


  postbuttonClick() {

    this.submitted = true;
    var formVal = this.addPostForm.value;

    // stop here if form is invalid
    if (this.addPostForm.invalid) { 
      return;
    } 
    this.waiting = true;

    // Initialize Params Object
    let formData = new FormData();

    formData.append('comment', formVal.comments);

    this.http.post<ResultVM>(this.baseUrl + 'api/gappdata/AddPost', formData)
      .subscribe(result => {
        this.resultVM = result;
        this.waiting = false;

        if (result.resultCode == "1") {

          this.submitted = false;
          this.followingService.refreshData.next(true);
          this.addPostForm.setValue({
            comments: ''
          });          
          //this.router.navigate(['/mypost']);
        }
        

      }, error => console.error(error));
  }

  //end class
}


interface ResultVM {
  resultCode: string,
  returnData: string

}


interface UserInfo {
  displayName: string,
  identifier: string
}
