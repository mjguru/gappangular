import { Component, Inject, Input } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { FollowingService } from '../../service/following.service';

@Component({
  selector: 'app-post-comment',
  templateUrl: './post-comment.component.html'
})
export class PostCommentsComponent {
  public postcomments: PostComment[];
  resultVM: ResultVM; 
  commentForm: FormGroup;
  submitted = false;
  waiting = false;
  totalRecords = 0;
  pageNumber = 1;
  rowCount = 0;
  displayName: string;

  @Input() postidentifier: string;

  ngOnInit() {

    this.resultVM = { resultCode: '', returnData: '' };

    this.commentForm = this.formBuilder.group({
      comments: ['', [Validators.required, Validators.maxLength(500)]]
    }
    );

    this.followingService.identifier.subscribe(identifier => this.onServiceResult(identifier));


    this.loadData();
  }

  constructor(private followingService: FollowingService, private formBuilder: FormBuilder,
    private http: HttpClient, @Inject('BASE_URL') private baseUrl: string) {
     

    this.http.get<UserInfo>(this.baseUrl + 'api/gappdata/GetLoggedInUserInfo')
      .subscribe(result => {
        this.displayName = result.displayName;
      }, error => console.error(error));

    
  }

  onServiceResult(identifier) {
    if (identifier) {

      this.postidentifier = identifier;

      this.waiting = false;
      this.totalRecords = 0;
      this.pageNumber = 1;
      this.rowCount = 0;
      this.postcomments = [];

      this.loadData();
    }
  }

  loadData() {

    if (this.postidentifier != undefined && this.postidentifier != '') {


      let params = new HttpParams();
      params = params.append('postIdentifier', this.postidentifier);
      params = params.append('pageNumber', String(this.pageNumber));



      this.waiting = true;

      this.http.get<PostComment[]>(this.baseUrl + 'api/gappdata/GetPostComments', { params: params })
        .subscribe(result => {
           

          if (this.postcomments == undefined) {
            this.postcomments = result;
          }
          else {
            for (var i = 0, len = result.length; i < len; i++) {
              this.postcomments.push(result[i]);
            }
          }

          this.totalRecords = this.postcomments.length;
          this.pageNumber += 1;


          if (this.postcomments.length > 0) {
            this.rowCount = this.postcomments[0].rowCount;
          }
          this.waiting = false;

        }, error => console.error(error));
    }
  }

  get result() {
    return this.resultVM;
  }

  get f() {
    return this.commentForm.controls;
  }


  loadMoreData() {
    this.loadData();
  }

  reloadData() {
    this.submitted = false;
    this.commentForm.setValue({
      comments: ''
    });
     
    this.postcomments = [];
    this.totalRecords = 0;
    this.pageNumber = 1;
    this.rowCount = 0;

    this.loadData();
  }

  postbuttonClick() {
    this.submitted = true;
    var formVal = this.commentForm.value;

    if (this.commentForm.invalid) {
      return;
    }
    this.waiting = true;
     
    let formData = new FormData();

    formData.append('postIdentifier', this.postidentifier);
    formData.append('comments', formVal.comments);

    this.http.post<ResultVM>(this.baseUrl + 'api/gappdata/AddPostComment', formData)
      .subscribe(result => {
        this.resultVM = result;
        this.reloadData();
        this.waiting = false;
      }, error => console.error(error));
  }

  //end class
}

interface PostComment {
  createdUserIdentifier: string,
  comment: string,
  createdOn: string,
  createdByUser: string,
  rowCount: number

}
interface ResultVM {
  resultCode: string,
  returnData: string

}

interface UserInfo {
  displayName: string,
  identifier: string
}
