import { Component, Inject, OnInit } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { FormControl, FormGroup, FormBuilder, Validators } from '@angular/forms'
import { Router } from '@angular/router';
import { FollowingService } from '../../service/following.service';

@Component({
  selector: 'app-followed',
  templateUrl: './followed.component.html'
})

export class FollowedComponent implements OnInit {
   

  waiting = false;
  totalRecords = 0;
  pageNumber = 1;
  rowCount = 0;

  public followers: FollowerVM[];

  ngOnInit() {
    this.loadData();

    this.followingService.refreshData.subscribe(refresh => this.onServiceResult(refresh));
  }

  constructor(private followingService: FollowingService, private http: HttpClient, @Inject('BASE_URL') private baseUrl: string) {
     

  }

  onServiceResult(refreshData) {
    if (refreshData) {

      this.waiting = false;
      this.totalRecords = 0;
      this.pageNumber = 1;
      this.rowCount = 0;
      this.followers = [];

      this.loadData();
    }
  }

  loadData() {

    let params = new HttpParams();
    params = params.append('type', "beingfollowed");
    params = params.append('pageNumber', this.pageNumber.toString());
   
    this.waiting = true;

    this.http.get<FollowerVM[]>(this.baseUrl + 'api/gappdata/GetFollowerList', { params: params })
      .subscribe(result => {

      if (this.followers == undefined) {
        this.followers = result;
      }
      else {
        for (var i = 0, len = result.length; i < len; i++) {
          this.followers.push(result[i]);
        }
      }

      this.totalRecords = this.followers.length;
      this.pageNumber += 1;


      if (this.followers.length > 0) {
        this.rowCount = this.followers[0].rowCount;
      }
      this.waiting = false;


    }, error => console.error(error));
  }

  loadMoreData() {
    this.loadData();
  }

  //end class
}

interface FollowerVM {

  username: string,
  followingSince: string,
  userIdentifier: string,
  rowCount: number

}
