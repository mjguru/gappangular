import { Component, Inject, Input } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { FollowingService } from '../../service/following.service';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html'
})
export class SidebarComponent { 

  waiting = false; 

  public posts: Post[];

  ngOnInit() { 
    this.followingService.refreshData.subscribe(refresh => this.onServiceResult(refresh));
    this.loadData();
  }

  constructor(private followingService: FollowingService, private http: HttpClient, @Inject('BASE_URL') private baseUrl: string) {
     
     
  }

  onServiceResult(refreshData) {
    if (refreshData) {

      this.waiting = false;
      this.posts = [];

      this.loadData();
    }
  }

  loadData() {
     

    this.waiting = true;

    this.http.get<Post[]>(this.baseUrl + 'api/gappdata/GetSidebarPosts').subscribe(result => {

      this.posts = result;
      this.waiting = false;
      //this.followingService.refreshData.next(false);

    }, error => console.error(error));
  }
   

  //end class
}

interface Post {

  createdUserIdentifier: string,
  postIdentifier: string,
  postTitle: string,
  postContent: string,
  createdOn: string,
  createdByUser: string,
  rowCount: number

}
