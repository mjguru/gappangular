import { Component, Inject, OnInit, Input } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { FormControl, FormGroup, FormBuilder, Validators } from '@angular/forms'
import { Router, ActivatedRoute } from '@angular/router';
import { FollowingService } from '../../service/following.service';

@Component({
  selector: 'app-followuser',
  templateUrl: './followuser.component.html'
})
export class FollowUserComponent implements OnInit {

  @Input() createdUserIdentifier: string;

  displayName: string; 
  resultVM: ResultVM;
  waiting = false; 
  isFollowed = false; 

  constructor(private followingService: FollowingService, private formBuilder: FormBuilder,
    private http: HttpClient, @Inject('BASE_URL') private baseUrl: string) {
     
    this.http.get<UserInfo>(this.baseUrl + 'api/gappdata/GetLoggedInUserInfo')
      .subscribe(result => {
        this.displayName = result.displayName;
      }, error => console.error(error));



  }

  ngOnInit() {

    this.resultVM = { resultCode: '', returnData: '' };


    let params = new HttpParams();
    params = params.append('postCreatedUserIdentifier', this.createdUserIdentifier);


    this.http.get<ResultVM>(this.baseUrl + 'api/gappdata/CheckIfUserIsBeingFollowed', { params: params })
      .subscribe(result => {

        this.isFollowed = (result.resultCode == "1");

      }, error => console.error(error));

  }

  get result() {
    return this.resultVM;
  }

  followClick() { 
    this.waiting = true;

    // Initialize Params Object
    let formData = new FormData();

    formData.append('userIdBeingFollowedIdentifier', this.createdUserIdentifier);

    let shouldBeFollowing = "no";
    let localFollowVariable = !this.isFollowed;

    if (localFollowVariable) {
      shouldBeFollowing = "yes";
    }
    formData.append('follow', shouldBeFollowing);

    this.http.post<ResultVM>(this.baseUrl + 'api/gappdata/AddRemoveFollower', formData)
      .subscribe(res => {
        this.resultVM = res;
        this.waiting = false;

        if (res.resultCode == "1") {
          this.isFollowed = localFollowVariable;
        }

        this.followingService.refreshData.next(true);
        this.followingService.refreshAllPost.next(true);
        
        
      }, error => console.error(error));
  }

  //end class
}


interface ResultVM {
  resultCode: string,
  returnData: string

}

interface UserInfo {
  displayName: string,
  identifier: string
}
