import { Component, Inject, OnInit } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { FormControl, FormGroup, FormBuilder, Validators } from '@angular/forms'

@Component({
  selector: 'app-forgotpassword',
  templateUrl: './forgotpassword.component.html'
})

export class ForgotPasswordComponent implements OnInit {


  resultVM: ResultVM;

  forgotPasswordForm: FormGroup;
  submitted = false;
  waiting = false; 
   

  constructor(private formBuilder: FormBuilder, private http: HttpClient, @Inject('BASE_URL') private baseUrl: string) {
     
  }


  ngOnInit() {

    this.resultVM = { resultCode: '', returnData: '' };

    this.forgotPasswordForm = this.formBuilder.group({
      username: ['', [Validators.required]],
      securitykey: ['', [Validators.required]],
      password: ['', [Validators.required, Validators.minLength(6)]],
      confirmpassword: ['', [Validators.required, Validators.minLength(6)]] 
    },
      { validator: this.checkIfMatchingPasswords('password', 'confirmpassword') }
    );

  }

  checkIfMatchingPasswords(passwordKey: string, passwordConfirmationKey: string) {
    return (group: FormGroup) => {
      let passwordInput = group.controls[passwordKey],
        passwordConfirmationInput = group.controls[passwordConfirmationKey];
      if (passwordInput.value !== passwordConfirmationInput.value) {
        return passwordConfirmationInput.setErrors({ notEquivalent: true })
      }
      else {
        return passwordConfirmationInput.setErrors(null);
      }
    }
  }

  get result() {
    return this.resultVM;
  }

  get f() {
    return this.forgotPasswordForm.controls;
  }

  reset() {
    this.submitted = false;
    this.waiting = false;

    this.forgotPasswordForm.setValue({
      username: '',
      password: '',
      confirmpassword: '',
      securitykey: ''
    });
  }

  forgotClick() {

    this.submitted = true;
    var formVal = this.forgotPasswordForm.value;

    // stop here if form is invalid
    if (this.forgotPasswordForm.invalid) {
      return;
    }

    this.waiting = true;

    // Initialize Params Object
    let formData = new FormData();

    formData.append('username', formVal.username);    
    formData.append('password', formVal.password);
    formData.append('securityKey', formVal.securitykey);

    this.http.post<ResultVM>(this.baseUrl + 'api/gappdata/ResetPassword', formData)
      .subscribe(result => {
        this.resultVM = result;
        this.waiting = false;

        if (this.resultVM.resultCode == "1") {
          this.reset();
        }
        

      }, error => console.error(error));
  }

  //end class
}




interface ResultVM {
  resultCode: string,
  returnData: string

}
