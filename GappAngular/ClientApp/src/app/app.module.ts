import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule  } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { RouterModule } from '@angular/router';

import { AppComponent } from './app.component';
import { NavMenuComponent } from './nav-menu/nav-menu.component';
import { HomeComponent } from './home/home.component';
import { AllPostComponent } from './all-post/all-post.component';
import { PostCommentsComponent } from './post-comment/post-comment.component';
import { RegisterComponent } from './register/register.component';
import { LoginComponent } from './login/login.component';
import { LogOffComponent } from './logoff/logoff.component';
import { AddPostComponent } from './addpost/addpost.component';
import { MyPostComponent } from './mypost/mypost.component';
import { PostListComponent } from './postlist/postlist.component';
import { EditPostComponent } from './editpost/editpost.component';
import { DeletePostComponent } from './deletepost/deletepost.component';
import { FollowUserComponent } from './followuser/followuser.component';
import { UserPostComponent } from './userpost/userpost.component';
import { ForgotPasswordComponent } from './forgotpassword/forgotpassword.component';
import { FollowingComponent } from './following/following.component';
import { FollowedComponent } from './followed/followed.component';
import { AuthenticationService } from '../service/auth.service '; 
import { FollowingService } from '../service/following.service';
import { SidebarComponent } from './sidebar/sidebar.component';
import { ViewPostComponent } from './viewpost/viewpost.component';
import { ContactComponent } from './contact/contact.component';
import { TermsComponent } from './terms/terms.component';
import { CustomHttpXsrfInterceptor } from '../service/CustomHttpXsrfInterceptor';
import { FollowCountComponent } from './followcount/followcount.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ScrollTopComponent } from './scrolltop/scrolltop.component';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';

@NgModule({
  declarations: [
    AppComponent,
    NavMenuComponent,
    HomeComponent, 
    AllPostComponent,
    PostCommentsComponent,
    RegisterComponent,
    LoginComponent,
    LogOffComponent,
    AddPostComponent,
    MyPostComponent,
    PostListComponent,
    EditPostComponent,
    DeletePostComponent,
    FollowUserComponent,
    UserPostComponent,
    ForgotPasswordComponent,
    FollowingComponent,
    FollowedComponent,
    SidebarComponent,
    ViewPostComponent,
    ContactComponent,
    TermsComponent,
    FollowCountComponent,
    ScrollTopComponent,
  ],
  imports: [
    BrowserModule.withServerTransition({ appId: 'ng-cli-universal' }), 
    HttpClientModule, 
    BrowserAnimationsModule, 
    FormsModule,
    ReactiveFormsModule,
    FontAwesomeModule,
    RouterModule.forRoot([
      { path: '', component: HomeComponent, pathMatch: 'full' }, 
      { path: 'all-post', component: AllPostComponent },
      { path: 'post-comment', component: PostCommentsComponent },
      { path: 'register', component: RegisterComponent },
      { path: 'login', component: LoginComponent },
      { path: 'logoff', component: LogOffComponent },
      { path: 'addpost', component: AddPostComponent },
      { path: 'mypost', component: MyPostComponent },
      { path: 'postlist', component: PostListComponent },
      { path: 'editpost/:id', component: EditPostComponent },
      { path: 'deletepost/:id', component: DeletePostComponent },
      { path: 'followuser', component: FollowUserComponent },
      { path: 'userpost/:id', component: UserPostComponent },
      { path: 'forgotpassword', component: ForgotPasswordComponent },
      { path: 'following', component: FollowingComponent },
      { path: 'followed', component: FollowedComponent },
      { path: 'sidebar', component: SidebarComponent },
      { path: 'viewpost/:id', component: ViewPostComponent },
      { path: 'contact', component: ContactComponent },
      { path: 'terms', component: TermsComponent },
      { path: 'followcount', component: FollowCountComponent },
      { path: 'scrolltop', component: ScrollTopComponent },
    ]),
     
  ],
  providers:
    [AuthenticationService, FollowingService      
     , { provide: HTTP_INTERCEPTORS, useClass: CustomHttpXsrfInterceptor, multi: true }

  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
