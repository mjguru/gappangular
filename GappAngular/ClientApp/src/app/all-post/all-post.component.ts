import { Component, Inject, Input } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { FollowingService } from '../../service/following.service';

@Component({
  selector: 'app-all-post',
  templateUrl: './all-post.component.html'
})
export class AllPostComponent {

  @Input() postIdentifier: string; 
  @Input() showMyPost: string;
  @Input() showFollowingPost: string;
  @Input() userIdentifer: string;
   

  waiting = false;
  totalRecords = 0;
  pageNumber = 1;
  rowCount = 0;
  
  loggedInUserIdentifier: string;

  public posts: Post[];

  ngOnInit() {
    this.followingService.refreshData.next(true);
    this.loadData();
  }

  constructor(private followingService: FollowingService, private http: HttpClient, @Inject('BASE_URL') private baseUrl: string) {
     

    http.get<UserInfo>(baseUrl + 'api/gappdata/GetLoggedInUserInfo')
      .subscribe(result => {

        this.loggedInUserIdentifier = result.identifier;

      }, error => console.error(error));


    this.followingService.refreshAllPost.subscribe(refresh => this.onServiceResult(refresh));

  }

  onServiceResult(refreshData) {
 
    if (refreshData) {
      
      this.waiting = false;
      this.totalRecords = 0;
      this.pageNumber = 1;
      this.rowCount = 0;
      this.posts = [];

      this.loadData();
    }
  }

  loadData() {
    
    let params = new HttpParams();
  
    params = params.append('postIdentifier', this.postIdentifier);
    params = params.append('pageNumber', this.pageNumber.toString());
    params = params.append('showMyPost', this.showMyPost);
    params = params.append('showFollowingPost', this.showFollowingPost);
    params = params.append('userIdentifer', this.userIdentifer);

    this.waiting = true;

    this.http.get<Post[]>(this.baseUrl + 'api/gappdata/GetPostByFilters', { params: params }).subscribe(result => {

      if (this.posts == undefined) {
        this.posts = result;
      }
      else {
        for (var i = 0, len = result.length; i < len; i++) {
          this.posts.push(result[i]);
        }
      }
      
      this.totalRecords = this.posts.length;
      this.pageNumber += 1;


      if (this.posts.length > 0) {
        this.rowCount = this.posts[0].rowCount;
      } 
      this.waiting = false;
      //this.followingService.refreshData.next(false);
      //this.followingService.refreshAllPost.next(false);

    }, error => console.error(error));
  }

  loadMoreData() { 
    this.loadData();    
  }

  //end class
}

interface Post {
  
    createdUserIdentifier : string,
    postIdentifier : string,
    postTitle: string,
    postContent: string,
    createdOn: string,
    createdByUser: string,
    rowCount: number

}

interface UserInfo {
  displayName: string,
  identifier: string
}
