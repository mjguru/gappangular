import { Component, Inject, OnInit } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { FormControl, FormGroup, FormBuilder, Validators } from '@angular/forms'
import { Router, ActivatedRoute } from '@angular/router';
import { FollowingService } from '../../service/following.service';

@Component({
  selector: 'app-deletepost',
  templateUrl: './deletepost.component.html'
})
export class DeletePostComponent implements OnInit {
  
  public posts: Post[];
  resultVM: ResultVM; 
  waiting = false;
  postIdentifier: string;
  postContent: string;
   

  constructor(private followingService: FollowingService, private route: ActivatedRoute, private router: Router,
    private formBuilder: FormBuilder, private http: HttpClient, @Inject('BASE_URL') private baseUrl: string) {

    this.postIdentifier = this.route.snapshot.params['id'];
    this.waiting = true; 

    let params = new HttpParams();
    params = params.append('postIdentifier', this.postIdentifier);

    this.http.get<Post[]>(this.baseUrl + 'api/gappdata/GetPostByFilters', { params: params }).subscribe(result => {
      this.posts = result;

      if (this.posts.length > 0) {
        this.postContent = this.posts[0].postContent;
      }

      this.waiting = false;

    }, error => console.error(error));

  }

  ngOnInit() {

    this.resultVM = { resultCode: '', returnData: '' };
     
  }

  get result() {
    return this.resultVM;
  }
    
  deletebuttonClick() {
      
    this.waiting = true;

    // Initialize Params Object
    let formData = new FormData();
     
    formData.append('identifier', this.postIdentifier);

    this.http.post<ResultVM>(this.baseUrl + 'api/gappdata/DeletePost', formData)
      .subscribe(result => {
        this.resultVM = result;
        this.waiting = false;
        this.followingService.refreshData.next(true);
      }, error => console.error(error));
  }

  //end class
}


interface ResultVM {
  resultCode: string,
  returnData: string

}



interface Post {

  createdUserIdentifier: string,
  postIdentifier: string,
  postTitle: string,
  postContent: string,
  createdOn: string,
  createdByUser: string,
  rowCount: number

}

