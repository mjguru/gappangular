import { Component, Inject, OnInit } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http'; 
import { Router } from '@angular/router';
import { AuthenticationService } from '../../service/auth.service ';

@Component({
  selector: 'app-logoff',
  templateUrl: './logoff.component.html'
})

export class LogOffComponent {
  
  constructor(private auth: AuthenticationService, private router: Router,http: HttpClient, @Inject('BASE_URL') baseUrl: string) {

    http.get<ResultVM>(baseUrl + 'api/gappdata/logoff')
      .subscribe(result => {
        auth.isLoggedIn.next(false);
        //location.href = '/';
        this.router.navigate(['/']);
      }, error => console.error(error));
  }


  //end class
}




interface ResultVM {
  resultCode: string,
  returnData: string

}
