import { Component, Inject, Input } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { AuthenticationService } from '../../service/auth.service ';

@Component({
  selector: 'app-nav-menu',
  templateUrl: './nav-menu.component.html',
  styleUrls: ['./nav-menu.component.css']
})
export class NavMenuComponent {
  isExpanded = false;
  displayName: string; 
  userInfo: UserInfo; 

  constructor(private auth: AuthenticationService, private http: HttpClient, @Inject('BASE_URL') private baseUrl: string) {
    this.displayName = '';  

    this.http.get<UserInfo>(this.baseUrl + 'api/gappdata/GetLoggedInUserInfo')
      .subscribe(result => {
        this.displayName = result.displayName;
      }, error => console.error(error));
  }

  ngOnInit() { 
    this.auth.isLoggedIn.subscribe(isLoggedIn => this.displayLoggedInfo(isLoggedIn)); 
  }


  displayLoggedInfo(isLoggedIn) {
     
    if (isLoggedIn) {
      this.http.get<UserInfo>(this.baseUrl + 'api/gappdata/GetLoggedInUserInfo')
        .subscribe(result => {
          this.displayName = result.displayName;
        }, error => console.error(error));
    }
    else {
      this.displayName = '';
    }
  }

  collapse() {
    this.isExpanded = false;
  }

  toggle() {
    this.isExpanded = !this.isExpanded;
  }  
}

interface UserInfo {
  displayName: string,
  identifier : string
}
