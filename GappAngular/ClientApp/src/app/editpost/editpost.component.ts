import { Component, Inject, OnInit } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { FormControl, FormGroup, FormBuilder, Validators } from '@angular/forms'
import { Router, ActivatedRoute } from '@angular/router';
import { FollowingService } from '../../service/following.service';

@Component({
  selector: 'app-editpost',
  templateUrl: './editpost.component.html'
})
export class EditPostComponent implements OnInit {

  public posts: Post[];
  resultVM: ResultVM;
  editPostForm: FormGroup;
  submitted = false;
  waiting = false;

  postIdentifier : string;
   

  constructor(private followingService: FollowingService, private route: ActivatedRoute, private router: Router,
    private formBuilder: FormBuilder, private http: HttpClient, @Inject('BASE_URL') private baseUrl: string) {
     
    this.postIdentifier = this.route.snapshot.params['id']; 
    this.waiting = true; 
    
    let params = new HttpParams();
    params = params.append('postIdentifier', this.postIdentifier);

    this.http.get<Post[]>(this.baseUrl + 'api/gappdata/GetPostByFilters', { params: params }).subscribe(result => {
      this.posts = result;

      if (this.posts.length > 0) {
        let content = this.posts[0].postContent;

        this.editPostForm.setValue({
          comments: content
        });

      }
      this.waiting = false;
       

    }, error => console.error(error));

  }

  ngOnInit() {

    this.resultVM = { resultCode: '', returnData: '' };


    this.editPostForm = this.formBuilder.group({
          comments: ['', [Validators.required, Validators.minLength(100)]]
        }
    );

  }

  get result() {
    return this.resultVM;
  }

  get f() {
    return this.editPostForm.controls;
  }


  postbuttonClick() {

    this.submitted = true;
    var formVal = this.editPostForm.value;

    // stop here if form is invalid
    if (this.editPostForm.invalid) {
      return;
    }
    this.waiting = true;

    // Initialize Params Object
    let formData = new FormData();

    formData.append('comment', formVal.comments);
    formData.append('identifier', this.postIdentifier);

    this.http.post<ResultVM>(this.baseUrl + 'api/gappdata/AddPost', formData)
      .subscribe(result => {
        this.resultVM = result;
        this.followingService.refreshData.next(true);
        this.waiting = false;
      }, error => console.error(error));
  }

  //end class
}


interface ResultVM {
  resultCode: string,
  returnData: string

}


interface Post {

  createdUserIdentifier: string,
  postIdentifier: string,
  postTitle: string,
  postContent: string,
  createdOn: string,
  createdByUser: string,
  rowCount: number

}
