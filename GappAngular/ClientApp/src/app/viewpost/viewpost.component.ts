import { Component, Inject, OnInit } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { FormControl, FormGroup, FormBuilder, Validators } from '@angular/forms'
import { Router, ActivatedRoute } from '@angular/router';
import { FollowingService } from '../../service/following.service';

@Component({
  selector: 'app-viewpost',
  templateUrl: './viewpost.component.html'
})
export class ViewPostComponent implements OnInit {

  waiting = false; 
  postContent: string;
  createdUserIdentifier: string;
  createdOn: string;
  createdByUser: string;
  postIdentifier: string;
  loggedInUserIdentifier: string;
   

  constructor(private followingService: FollowingService, private route: ActivatedRoute, private router: Router,
    private formBuilder: FormBuilder, private http: HttpClient, @Inject('BASE_URL') private baseUrl: string) {
  

    http.get<UserInfo>(baseUrl + 'api/gappdata/GetLoggedInUserInfo')
      .subscribe(result => {

        this.loggedInUserIdentifier = result.identifier;

      }, error => console.error(error));
  }

  ngOnInit() {
      
    this.waiting = true;    
    
    this.route.params.subscribe(
      p => {
        const id = p['id'];

        let params = new HttpParams();
        params = params.append('identifier', id);

        this.http.get<Post>(this.baseUrl + 'api/gappdata/GetPostByIdentifier', { params: params }).subscribe(result => {
          this.postContent = result.postContent;
          this.createdUserIdentifier = result.createdUserIdentifier;
          this.createdOn = result.createdOn;
          this.createdByUser = result.createdByUser;
          this.postIdentifier = result.postIdentifier;

          this.followingService.identifier.next(id);
          this.followingService.userIdentifier.next(result.createdUserIdentifier);

          

          this.waiting = false;

        }, error => console.error(error)); 
      }
    );

   
  }
    
   
  //end class
}


interface ResultVM {
  resultCode: string,
  returnData: string

}


interface Post {

  createdUserIdentifier: string,
  postIdentifier: string,
  postTitle: string,
  postContent: string,
  createdOn: string,
  createdByUser: string,
  rowCount: number

}


interface UserInfo {
  displayName: string,
  identifier: string
}
