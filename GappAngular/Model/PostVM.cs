﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GappAngular.Model
{
    public class PostVM
    {
        public Guid CreatedUserIdentifier { get; set; }

        public Guid PostIdentifier { get; set; }

        public string PostTitle { get; set; }

        public string PostContent { get; set; }

        public string CreatedOn { get; set; }

        public string CreatedByUser { get; set; }

        public int RowCount { get; set; }
    }
}
