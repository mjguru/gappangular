﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GappAngular.Model
{
    public class UserInfoVM
    {
        public string DisplayName { get; set; }

        public string Identifier { get; set; }
    }
}
