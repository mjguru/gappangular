﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace GappAngular.Model
{
    public class DataObject
    { 
        /// <summary>
        /// 
        /// </summary>
        /// <param name="pageNumber"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        public static DataSet GetPostsYouFollowing(int pageNumber, int pageSize)
        {
            var parameters = new List<SqlParameter>();
            DataSet dsFollow = new DataSet();

            if (Common.IsUserLoggedIn())
            {
                int userId = Common.GetLoggedInUserId();

                parameters.Add(SqlHelper.CreateSqlParameter("@UserFK", SqlDbType.Int, 8, userId));
                parameters.Add(SqlHelper.CreateSqlParameter("@PageNumber", SqlDbType.Int, 8, pageNumber));
                parameters.Add(SqlHelper.CreateSqlParameter("@PageSize", SqlDbType.Int, 8, pageSize));

                dsFollow = SqlHelper.GetDataSet("Post_PostsYouFollowing", parameters);
            }

            return dsFollow;

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="userIdentifier"></param>
        /// <param name="pageNumber"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        public static DataSet SearchUserPostData(Guid userIdentifier, int pageNumber, int pageSize)
        {
            var parameters = new List<SqlParameter>();
            DataSet dsFollow = new DataSet();

            //if (Common.IsUserLoggedIn())
            //{

                int userId = Common.GetLoggedInUserId();

                parameters.Add(SqlHelper.CreateSqlParameter("@UserIdentifier", SqlDbType.UniqueIdentifier, 100, userIdentifier));
                parameters.Add(SqlHelper.CreateSqlParameter("@PageNumber", SqlDbType.Int, 8, pageNumber));
                parameters.Add(SqlHelper.CreateSqlParameter("@PageSize", SqlDbType.Int, 8, pageSize));

                dsFollow = SqlHelper.GetDataSet("Post_SearchUserPosts", parameters);
            //}

            return dsFollow;

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="identifier"></param>
        /// <param name="pageNumber"></param>
        /// <param name="showMyPost"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        public static DataSet GetAllPosts(Guid? identifier, int? pageNumber, string showMyPost, int pageSize)
        {
            var parameters = new List<SqlParameter>();

            bool showOnlyMyPost = (!string.IsNullOrEmpty(showMyPost) && showMyPost.Trim().ToLower() == "yes");

            parameters.Add(SqlHelper.CreateSqlParameter("@Identifier", SqlDbType.UniqueIdentifier, 8000, identifier));
            parameters.Add(SqlHelper.CreateSqlParameter("@PageNumber", SqlDbType.Int, 8, pageNumber));
            parameters.Add(SqlHelper.CreateSqlParameter("@PageSize", SqlDbType.Int, 8, pageSize));

            if (showOnlyMyPost)
            {
                parameters.Add(SqlHelper.CreateSqlParameter("@ShowMyPost", SqlDbType.Bit, 8, showOnlyMyPost));
                parameters.Add(SqlHelper.CreateSqlParameter("@UserFK", SqlDbType.Int, 8, Common.GetLoggedInUserId()));
            }

            var dsComments = SqlHelper.GetDataSet("Post_GetAll", parameters);
            return dsComments;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="identifier"></param>
        /// <param name="pageNumber"></param>
        /// <returns></returns>
        public static DataSet PostCommentsLoadData(Guid identifier, int pageNumber, int pageSize)
        { 
            DataSet dsComments = new DataSet();
             
            var parameters = new List<SqlParameter>();

            parameters.Add(SqlHelper.CreateSqlParameter("@Identifier", SqlDbType.UniqueIdentifier, 8000, identifier));
            parameters.Add(SqlHelper.CreateSqlParameter("@PageNumber", SqlDbType.Int, 8, pageNumber));
            parameters.Add(SqlHelper.CreateSqlParameter("@PageSize", SqlDbType.Int, 8, pageSize));

            return SqlHelper.GetDataSet("Post_GetComment", parameters); 
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="vm"></param>
        /// <returns></returns>
        public static string CreateNewUser(string securityKey, string Username, string Password)
        {
            string result = "0";
            try
            {
                var parameters = new List<SqlParameter>();

                parameters.Add(SqlHelper.CreateSqlParameter("@Identifier", SqlDbType.UniqueIdentifier, 8000, Guid.NewGuid()));
                parameters.Add(SqlHelper.CreateSqlParameter("@Username", SqlDbType.VarChar, 8000, Username.ToString()));

                HashSalt hashSalt = EncryptionHelper.GenerateSaltedHash(64, Password.Trim());
                parameters.Add(SqlHelper.CreateSqlParameter("@Salt", SqlDbType.VarChar, 8000, hashSalt.Salt));
                parameters.Add(SqlHelper.CreateSqlParameter("@Hash", SqlDbType.VarChar, 8000, hashSalt.Hash));
                parameters.Add(SqlHelper.CreateSqlParameter("@SecurityKey", SqlDbType.VarChar, 8000, securityKey));

                parameters.Add(SqlHelper.CreateSqlParameter("@OutputVal", SqlDbType.VarChar, 8000, "", ParameterDirection.Output));

                result = SqlHelper.ExecuteProcedure("proc_NewUser", parameters);
            }
            catch (Exception ex)
            {

            }


            return result;

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="type"></param>
        /// <param name="pageNumber"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        public static DataSet GetFollowers(string type, int pageNumber, int pageSize)
        { 
            var parameters = new List<SqlParameter>();
            DataSet dsFollow = new DataSet();
            
            if (Common.IsUserLoggedIn())
            {

                parameters.Add(SqlHelper.CreateSqlParameter("@UserFK", SqlDbType.Int, 8, Common.GetLoggedInUserId()));
                parameters.Add(SqlHelper.CreateSqlParameter("@Action", SqlDbType.VarChar, 8000, type));
                parameters.Add(SqlHelper.CreateSqlParameter("@PageNumber", SqlDbType.Int, 8, pageNumber));
                parameters.Add(SqlHelper.CreateSqlParameter("@PageSize", SqlDbType.Int, 8, pageSize));

                dsFollow = SqlHelper.GetDataSet("Post_Follower", parameters);

            }

            return dsFollow;
        }

        public static DataSet GetPostByIdentifier(Guid identifier)
        {
            var parameters = new List<SqlParameter>(); 

            parameters.Add(SqlHelper.CreateSqlParameter("@Identifier", SqlDbType.UniqueIdentifier, 8000, identifier));

            return SqlHelper.GetDataSet("Post_GetByIdentifier", parameters); 
        }

    }
}
