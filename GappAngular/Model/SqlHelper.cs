﻿ 
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using System.Configuration;

namespace GappAngular.Model
{
    public class SqlHelper
    {
        //static string connectionString = @"server=MFM-IT-30Y8WC2\SQL2014;database=g;uid=sa;password=Mj89976*;";
        // static string connectionString = @"Data Source=.;Initial Catalog=g;Integrated Security=True;MultipleActiveResultSets=true;";
        static string connectionString = @"server=52.117.175.212,782;database=manu_john_g;uid=manu_john_g;password=Mj89976*;";



        /// <summary>
        /// 
        /// </summary>
        /// <param name="procedureName"></param>
        /// <param name="parms"></param>
        /// <returns></returns>
        public static DataSet GetDataSet(string procedureName, List<SqlParameter> parms = null)
        {
            DataSet ds = new DataSet();

            try
            {
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    conn.Open();

                    SqlCommand cmd = new SqlCommand(procedureName, conn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    if (parms != null && parms.Count() > 0)
                    {
                        foreach (var parameter in parms)
                        {
                            cmd.Parameters.Add(parameter);
                        }
                    }

                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    da.Fill(ds);

                    conn.Close();
                }

            }
            catch (Exception ex)
            {

            }

            return ds;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="procedureName"></param>
        /// <param name="parms"></param>
        /// <returns></returns>
        public static string ExecuteProcedure(string procedureName, List<SqlParameter> parms = null)
        {
            string outputVal = "0";

            try
            {
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    conn.Open();

                    SqlCommand cmd = new SqlCommand(procedureName, conn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    if (parms != null && parms.Count() > 0)
                    {
                        foreach (var parameter in parms)
                        {
                            cmd.Parameters.Add(parameter);
                        }
                    }

                    cmd.ExecuteNonQuery();
                    outputVal = cmd.Parameters["@OutputVal"].Value.ToString();

                    conn.Close();
                }

            }
            catch (Exception ex)
            {

            }

            return outputVal;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"></param>
        /// <param name="type"></param>
        /// <param name="size"></param>
        /// <param name="value"></param>
        /// <param name="direction"></param>
        /// <returns></returns>
        public static SqlParameter CreateSqlParameter(string name, SqlDbType type, int size, object value, ParameterDirection direction = ParameterDirection.Input)
        {
            var param = new SqlParameter(name, type, size);
            param.Value = value;
            param.Direction = direction;
            return param;

        }
    }
}
