﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GappAngular.Model
{
    public class CustomPrincipalSerializeModel
    {
        public int Id { get; set; }
        public string Username { get; set; }

    }
}
