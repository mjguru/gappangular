﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GappAngular.Model
{
    public class FollowerVM
    {
        public string Username { get; set; }
        public string FollowingSince { get; set; }

        public int RowCount { get; set; }

        public string UserIdentifier { get; set; }

    }
}
