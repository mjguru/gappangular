﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GappAngular.Model
{
    public class ResultVM
    {
        public string ResultCode { get; set; }

        public string ReturnData { get; set; }
    }
}
