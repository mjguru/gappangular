﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;

namespace GappAngular.Model
{
    public class Common
    {
        public static bool IsUserLoggedIn()
        {
            return HttpHelper.HttpContext.User.Identity.IsAuthenticated;

        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static int GetLoggedInUserId()
        {
            int userId = 0; 

            try
            {
                if (HttpHelper.HttpContext.User.Identity.IsAuthenticated)
                {
                    ClaimsIdentity claims = HttpHelper.HttpContext.User.Identity as ClaimsIdentity;

                    var claim = claims.FindFirst("UserId");
                    userId = (claim == null ? 0 : Convert.ToInt32(claim.Value));
                }

            }
            catch (Exception ex)
            {
            }

            return userId;
        }

        public static string GetLoggedInUserIdentifier()
        {
            string identifier = "";

            try
            {
                if (HttpHelper.HttpContext.User.Identity.IsAuthenticated)
                {
                    ClaimsIdentity claims = HttpHelper.HttpContext.User.Identity as ClaimsIdentity;

                    var claim = claims.FindFirst("identifier");
                    identifier = (claim == null ? "" : claim.Value);
                }

            }
            catch (Exception ex)
            {
            }

            return identifier;
        }

        public static string GetLoggedInUsername()
        {
            string username = ""; 

            try
            {
                if (HttpHelper.HttpContext.User.Identity.IsAuthenticated)
                {
                    ClaimsIdentity claims = HttpHelper.HttpContext.User.Identity as ClaimsIdentity;
                    username = claims.Name;
                }
            }
            catch (Exception ex)
            {
            }
            return username;
        }
    }
}
