﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Threading.Tasks;

namespace GappAngular.Model
{
    interface ICustomPrincipal : IPrincipal
    {
        int Id { get; set; }
        string Username { get; set; }

    }
}
