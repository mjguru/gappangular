﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GappAngular.Model
{
    public class PostCommentVM
    {
        public Guid CreatedUserIdentifier { get; set; }
        public string Comment { get; set; }

        public string CreatedOn { get; set; }

        public string CreatedByUser { get; set; }

        public int RowCount { get; set; }
    }
}
