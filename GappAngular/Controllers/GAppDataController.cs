﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using GappAngular.Model;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace GappAngular.Controllers
{
    [Route("api/[controller]")] 
    public class GAppDataController : ControllerBase
    {

        int _normalPageSize = 10;

        [HttpGet("[action]")]
        public List<PostVM> GetPostByFilters(Guid? postIdentifier, int? pageNumber, string showMyPost, 
                string showFollowingPost, Guid? userIdentifer)
        {
            var postList = new List<PostVM>();

            if (!pageNumber.HasValue)
            {
                pageNumber = 1;
            }

            var dsComments = new DataSet();

            if (!string.IsNullOrEmpty(showFollowingPost) && showFollowingPost.Trim().ToLower() == "yes")
            {
                dsComments = DataObject.GetPostsYouFollowing(pageNumber.Value, _normalPageSize);
            }
            else if (userIdentifer.HasValue)
            {
                dsComments = DataObject.SearchUserPostData(userIdentifer.Value, pageNumber.Value, _normalPageSize);
            }
            else
            {
                dsComments = DataObject.GetAllPosts(postIdentifier, pageNumber, showMyPost, _normalPageSize);
            } 
             

            if (dsComments != null && dsComments.Tables != null && dsComments.Tables.Count > 0)
            {
                if (dsComments.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow row in dsComments.Tables[0].Rows)
                    {
                        postList.Add(new PostVM()
                        {
                            CreatedUserIdentifier = (Guid)row["CreatedUserIdentifier"],
                            PostIdentifier = (Guid)row["PostIdentifier"],
                            PostTitle = row["PostTitle"].ToString().Trim(),
                            PostContent = row["PostContent"].ToString().Trim(),
                            CreatedOn = row["CreatedOn"].ToString().Trim(),
                            CreatedByUser = row["CreatedByUser"].ToString().Trim(),
                            RowCount = Convert.ToInt32(row["RowCount"].ToString().Trim()) 
                        });
                    }
                }
            }

            return postList;
        }

        [HttpGet("[action]")]
        public List<PostCommentVM> GetPostComments(Guid postIdentifier, int? pageNumber)
        {
            var postList = new List<PostCommentVM>();

            if (!pageNumber.HasValue)
            {
                pageNumber = 1;
            }

            int pageSize = 5;
            var dsComments =  DataObject.PostCommentsLoadData(postIdentifier, pageNumber.Value, pageSize); 

            if (dsComments != null && dsComments.Tables != null && dsComments.Tables.Count > 0)
            {
                if (dsComments.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow row in dsComments.Tables[0].Rows)
                    {
                        postList.Add(new PostCommentVM()
                        {
                            CreatedUserIdentifier = (Guid)row["CreatedUserIdentifier"],
                            Comment = row["PostComment"].ToString().Trim(),
                            CreatedOn = row["CreatedOn"].ToString().Trim(),
                            CreatedByUser = row["CreatedByUser"].ToString().Trim(),
                            RowCount = Convert.ToInt32(row["RowCount"].ToString().Trim())
                        });
                    }
                }
            }

            return postList;
        }

        [HttpPost("[action]")]
        
        public ResultVM CreateNewUser(string Username, string Password)
        {
            string securityKey = Username + Guid.NewGuid().ToString("N"); 

            var result = DataObject.CreateNewUser(securityKey,  Username,  Password);

            return new ResultVM() { ResultCode = result, ReturnData = securityKey };
        }

        [HttpPost("[action]")] 
        public ResultVM ValidateLogin(string Username, string Password)
        {
            string resultCode = "0";
            try
            {
                var parameters = new List<SqlParameter>();
                parameters.Add(SqlHelper.CreateSqlParameter("@Username", SqlDbType.VarChar, 8000, Username.Trim()));

                DataSet dsUser = SqlHelper.GetDataSet("proc_Login", parameters);
                if (dsUser != null && dsUser.Tables.Count > 0 && dsUser.Tables[0].Rows.Count > 0)
                {
                    string salt = dsUser.Tables[0].Rows[0]["Salt"].ToString();
                    string hash = dsUser.Tables[0].Rows[0]["Hash"].ToString();
                    int userId = Convert.ToInt32(dsUser.Tables[0].Rows[0]["Id"].ToString());
                    string identifier = dsUser.Tables[0].Rows[0]["Identifier"].ToString();

                    bool isPasswordValid = EncryptionHelper.VerifyPassword(Password.Trim(), hash, salt);

                    if (!isPasswordValid)
                    {
                        resultCode = "0";
                    }
                    else
                    {
                        var claims = new List<Claim>()
                        {
                            new Claim(ClaimTypes.Name, Username.Trim()),
                            new Claim("UserId", userId.ToString()),
                            new Claim("identifier", identifier.ToString()),
                            new Claim(ClaimTypes.Role,"Admin")
                        };

                        var claimsIdentity = new ClaimsIdentity(claims, CookieAuthenticationDefaults.AuthenticationScheme);
                        var claimsPrincipal = new ClaimsPrincipal(claimsIdentity);

                         HttpContext.SignInAsync(
                            CookieAuthenticationDefaults.AuthenticationScheme,
                            claimsPrincipal,
                            new AuthenticationProperties
                            {
                                IsPersistent = true,
                                ExpiresUtc = DateTime.UtcNow.AddMinutes(20)
                            });


                        HttpContext.User = claimsPrincipal;

                        resultCode = "1";
                    }
                }
                else
                {
                    resultCode = "0";
                }
            }
            catch (Exception ex)
            {
                 
            } 

            return new ResultVM() { ResultCode = resultCode };
        }

        [HttpGet("[action]")]
        public UserInfoVM GetLoggedInUserInfo()
        {
            UserInfoVM vm = new UserInfoVM();
            vm.DisplayName = Common.GetLoggedInUsername();
            vm.Identifier = Common.GetLoggedInUserIdentifier();

            return vm;
        }


        [HttpGet("[action]")]
        public ResultVM LogOff()
        {
            HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);

            HttpContext.User = new ClaimsPrincipal();

            return new ResultVM();
        }

        [HttpPost("[action]")]
        
        public ResultVM AddPost(string comment, Guid? identifier)
        {
            string resultCode = "0";

            if (Common.IsUserLoggedIn())
            {
                int userId = Common.GetLoggedInUserId();
                try
                {
                    var parameters = new List<SqlParameter>();

                    parameters.Add(SqlHelper.CreateSqlParameter("@Identifier", SqlDbType.UniqueIdentifier, 8000, identifier));
                    parameters.Add(SqlHelper.CreateSqlParameter("@UserFK", SqlDbType.Int, 8, userId));
                    parameters.Add(SqlHelper.CreateSqlParameter("@PostContent", SqlDbType.VarChar, 8000, comment.ToString()));
                    parameters.Add(SqlHelper.CreateSqlParameter("@OutputVal", SqlDbType.VarChar, 8000, "", ParameterDirection.Output));

                    resultCode = SqlHelper.ExecuteProcedure("Post_AddNew", parameters);
                }
                catch (Exception ex)
                {

                }
            }
            else
            {
                resultCode = "999";
            }

            return new ResultVM() { ResultCode = resultCode };
        }

        [HttpPost("[action]")]
        
        public ResultVM DeletePost(Guid identifier)
        {
            string resultCode = "0";

            if (Common.IsUserLoggedIn())
            {
                int userId = Common.GetLoggedInUserId();
                try
                {
                    var parameters = new List<SqlParameter>();

                    parameters.Add(SqlHelper.CreateSqlParameter("@Identifier", SqlDbType.UniqueIdentifier, 8000, identifier));
                    parameters.Add(SqlHelper.CreateSqlParameter("@UserFK", SqlDbType.Int, 8, userId));
                    parameters.Add(SqlHelper.CreateSqlParameter("@OutputVal", SqlDbType.VarChar, 8000, "", ParameterDirection.Output));

                    resultCode = SqlHelper.ExecuteProcedure("Post_Delete", parameters);
                }
                catch (Exception ex)
                {

                }
            }
            else
            {
                resultCode = "999";
            }

            return new ResultVM() { ResultCode = resultCode };
        }

        [HttpGet("[action]")]
        public ResultVM CheckIfUserIsBeingFollowed(Guid postCreatedUserIdentifier)
        {
            string resultCode = "0";

            if (Common.IsUserLoggedIn())
            { 
                try
                {
                    Guid loggedInUserGuid = new Guid(Common.GetLoggedInUserIdentifier());

                    var parameters = new List<SqlParameter>();

                    parameters.Add(SqlHelper.CreateSqlParameter("@CreatedUserIdentifier ", SqlDbType.UniqueIdentifier, 8000, postCreatedUserIdentifier));
                    parameters.Add(SqlHelper.CreateSqlParameter("@LoggedInUserIdentifier", SqlDbType.UniqueIdentifier, 8000, loggedInUserGuid));
                    parameters.Add(SqlHelper.CreateSqlParameter("@OutputVal", SqlDbType.VarChar, 8000, "", ParameterDirection.Output));

                    resultCode = SqlHelper.ExecuteProcedure("User_IsFollowing", parameters);
                }
                catch (Exception ex)
                {

                }
            } 

            return new ResultVM() { ResultCode = resultCode };
        }

        [HttpPost("[action]")]
        
        public ResultVM AddRemoveFollower(Guid userIdBeingFollowedIdentifier, string follow)
        {
            bool shouldFollow = (follow.Trim().ToLower() == "yes");

            string resultCode = "0";

            if (Common.IsUserLoggedIn())
            { 
                try
                {
                    Guid loggedInUserGuid = new Guid(Common.GetLoggedInUserIdentifier());
                    var parameters = new List<SqlParameter>();

                    parameters.Add(SqlHelper.CreateSqlParameter("@UserIdBeingFollowedIdentifier ", SqlDbType.UniqueIdentifier, 8000, userIdBeingFollowedIdentifier));
                    parameters.Add(SqlHelper.CreateSqlParameter("@UserIdWhoIsFollowingIdentifier", SqlDbType.UniqueIdentifier, 8000, loggedInUserGuid));
                    parameters.Add(SqlHelper.CreateSqlParameter("@Follow", SqlDbType.Bit, 8, Convert.ToByte(shouldFollow)));
                    parameters.Add(SqlHelper.CreateSqlParameter("@OutputVal", SqlDbType.VarChar, 8000, "", ParameterDirection.Output));

                    resultCode = SqlHelper.ExecuteProcedure("User_AddRemoveFollower", parameters);
                }
                catch (Exception ex)
                {

                }
            }
            else
            {
                resultCode = "999";
            }

            return new ResultVM() { ResultCode = resultCode };
        }


        [HttpPost("[action]")]
        
        public ResultVM AddPostComment(Guid postIdentifier, string comments)
        { 

            string resultCode = "0";

            if (Common.IsUserLoggedIn())
            {
                try
                { 
                    var parameters = new List<SqlParameter>();

                    parameters.Add(SqlHelper.CreateSqlParameter("@Identifier ", SqlDbType.UniqueIdentifier, 8000, postIdentifier));
                    parameters.Add(SqlHelper.CreateSqlParameter("@Comment", SqlDbType.VarChar, 8000, comments));
                    parameters.Add(SqlHelper.CreateSqlParameter("@CreatedBy", SqlDbType.BigInt, 8, Common.GetLoggedInUserId()));
                    parameters.Add(SqlHelper.CreateSqlParameter("@OutputVal", SqlDbType.VarChar, 8000, "", ParameterDirection.Output));

                    resultCode = SqlHelper.ExecuteProcedure("Post_AddComment", parameters);
                }
                catch (Exception ex)
                {

                }
            } 

            return new ResultVM() { ResultCode = resultCode };
        }


        [HttpPost("[action]")]
        
        public ResultVM ResetPassword(string username, string password, string securityKey)
        {

            string resultCode = "-1";

            try
            {
                var parameters = new List<SqlParameter>();
                 
                
                parameters.Add(SqlHelper.CreateSqlParameter("@Username", SqlDbType.VarChar, 8000, username.Trim().ToString()));

                HashSalt hashSalt = EncryptionHelper.GenerateSaltedHash(64, password.Trim());

                parameters.Add(SqlHelper.CreateSqlParameter("@Salt", SqlDbType.VarChar, 8000, hashSalt.Salt));
                parameters.Add(SqlHelper.CreateSqlParameter("@Hash", SqlDbType.VarChar, 8000, hashSalt.Hash));
                parameters.Add(SqlHelper.CreateSqlParameter("@SecurityKey", SqlDbType.VarChar, 8000, securityKey));
                parameters.Add(SqlHelper.CreateSqlParameter("@OutputVal", SqlDbType.VarChar, 8000, "", ParameterDirection.Output));

                resultCode = SqlHelper.ExecuteProcedure("User_ResetPassword", parameters);
            }
            catch (Exception ex)
            {

            }

            return new ResultVM() { ResultCode = resultCode };
        }

        [HttpGet("[action]")]
        public List<FollowerVM> GetFollowerList(string type, int? pageNumber)
        {
            var followerList = new List<FollowerVM>();

            if (!pageNumber.HasValue)
            {
                pageNumber = 1;
            }

            var dsFollower = DataObject.GetFollowers(type, pageNumber.Value, _normalPageSize);
            if (dsFollower != null && dsFollower.Tables != null && dsFollower.Tables.Count > 0)
            {
                if (dsFollower.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow row in dsFollower.Tables[0].Rows)
                    {
                        followerList.Add(new FollowerVM()
                        {
                            Username = row["Username"].ToString().Trim(),
                            FollowingSince = row["FollowingSince"].ToString().Trim(),
                            RowCount = Convert.ToInt32(row["RowCount"].ToString().Trim()),
                            UserIdentifier = row["UserIdentifier"].ToString().Trim()
                        });
                    }
                }
            }

            return followerList;
        }

        [HttpGet("[action]")]
        public List<PostVM> GetSidebarPosts()
        {
            var postList = new List<PostVM>();
            int pageNumber = 1;
            int pageSize = 5; 

            var dsComments = DataObject.GetAllPosts(null, pageNumber, "", pageSize);


            if (dsComments != null && dsComments.Tables != null && dsComments.Tables.Count > 0)
            {
                if (dsComments.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow row in dsComments.Tables[0].Rows)
                    {
                        string postContent = row["PostContent"].ToString().Trim(); 
                        postList.Add(new PostVM()
                        {
                            CreatedUserIdentifier = (Guid)row["CreatedUserIdentifier"],
                            PostIdentifier = (Guid)row["PostIdentifier"],
                            PostTitle = row["PostTitle"].ToString().Trim(),
                            PostContent = postContent,
                            CreatedOn = row["CreatedOn"].ToString().Trim(),
                            CreatedByUser = row["CreatedByUser"].ToString().Trim(),
                            RowCount = Convert.ToInt32(row["RowCount"].ToString().Trim())
                        });
                    }
                }
            }

            return postList;
        }

        [HttpGet("[action]")]
        public PostVM GetPostByIdentifier(Guid identifier)
        {
            var postList = new PostVM();

            var dsComments = DataObject.GetPostByIdentifier(identifier);

            if (dsComments != null && dsComments.Tables != null && dsComments.Tables.Count > 0)
            {
                if (dsComments.Tables[0].Rows.Count > 0)
                {
                    DataRow row = dsComments.Tables[0].Rows[0];

                    postList = new PostVM()
                    {
                        CreatedUserIdentifier = (Guid)row["CreatedUserIdentifier"],
                        PostIdentifier = (Guid)row["PostIdentifier"],
                        PostTitle = row["PostTitle"].ToString().Trim(),
                        PostContent = row["PostContent"].ToString().Trim(),
                        CreatedOn = row["CreatedOn"].ToString().Trim(),
                        CreatedByUser = row["CreatedByUser"].ToString().Trim(),
                        RowCount = Convert.ToInt32(row["RowCount"].ToString().Trim())
                    };
                }
            }

            return postList;
        }


        [HttpGet("[action]")]
        public List<FollowerVM> GetFollowedByCount(Guid userIdentifer)
        {
            var followerList = new List<FollowerVM>();
            var parameters = new List<SqlParameter>();
            DataSet dsFollower = new DataSet();

            parameters.Add(SqlHelper.CreateSqlParameter("@UserIdentifier", SqlDbType.UniqueIdentifier, 100, userIdentifer));
            dsFollower = SqlHelper.GetDataSet("Post_GetBeingFollowedCount", parameters);
            
            if (dsFollower != null && dsFollower.Tables != null && dsFollower.Tables.Count > 0)
            {
                if (dsFollower.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow row in dsFollower.Tables[0].Rows)
                    {
                        followerList.Add(new FollowerVM()
                        { 
                            RowCount = Convert.ToInt32(row["RowCount"].ToString().Trim())
                          
                        });
                    }
                }
            }

            return followerList;
        }

    }     
}